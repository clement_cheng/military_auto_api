# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from military_auto_server.models.quote import Quote  # noqa: E501
from military_auto_server.test import BaseTestCase


class TestQuoteController(BaseTestCase):
    """QuoteController integration test stubs"""

    def test_get_quote(self):
        """Test case for get_quote

        Get a specific quote by quote id
        """
        response = self.client.open(
            '/abu88/millitary_auto/1.0.0/quote/{quoteid}'.format(quoteid='quoteid_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_quotes(self):
        """Test case for get_quotes

        Get all quotes or specific quote
        """
        query_string = [('quoteid', 'quoteid_example'),
                        ('skip', 1),
                        ('limit', 50)]
        response = self.client.open(
            '/abu88/millitary_auto/1.0.0/quotes',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_quote(self):
        """Test case for quote

        Make a quote for Military Auto product
        """
        body = Quote()
        response = self.client.open(
            '/abu88/millitary_auto/1.0.0/quote',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_quote(self):
        """Test case for update_quote

        Update a specific quote by quote id
        """
        response = self.client.open(
            '/abu88/millitary_auto/1.0.0/quote/{quoteid}'.format(quoteid='quoteid_example'),
            method='PUT')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
