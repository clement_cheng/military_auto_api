import connexion
import six
import urllib.request, ssl
import xml.etree.ElementTree as ET
from xml.dom import minidom
import re

from military_auto_server.models.quote import Quote  # noqa: E501
from military_auto_server import util

def setElementRequest(elem, field, value):
    ET.SubElement(elem, 'ManuScript.setValueRq', {'manuscript': '~ManuScriptID~', 'field': field, 'value': value})

def getElementRequest(elem, field):
    ET.SubElement(elem, 'ManuScript.getValueRq', {'manuscript': '~ManuScriptID~', 'field': field})

def setVariablesRequest(ET, requests, manuscript, body, save=False):
    
    effectiveDate = body['pol_term_from_dt']
    effectiveDate = re.sub('[/]', '-', effectiveDate)

    # set variable
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'ManuScriptID','value': manuscript})
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'EffectiveDate','value': effectiveDate})
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'PolicyHolderLastName','value': body['ins_name']})

    # online datsa
    ET.SubElement(requests, 'OnlineData.newPolicyRq', {'manuscript': '~ManuScriptID~', 'addProducerInfo': '1'})

    # account input
    setElementRequest(requests, 'AccountInput.BirthDate', str(body['ins_birthdate']))
    setElementRequest(requests, 'AccountInput.MaritalStatus', str(body['ins_marital']))
    setElementRequest(requests, 'AccountInput.MilitaryRank', str(body['ins_rank']))
    setElementRequest(requests, 'AccountInput.MilitaryBaseLocation', str(body['veh_lic_plate2']))

    if save == True:
        setElementRequest(requests, 'AccountInput.Address1', str(body['ins_addr1']))
        setElementRequest(requests, 'AccountInput.Address2', str(body['ins_addr2']))
        # setElementRequest(requests, 'AccountInput.PrimaryPhone', str(body['ins_tel_home']))

    # policy holder name
    setElementRequest(requests, 'AccountInput.LastName', '~PolicyHolderLastName~')

    # vehicle Input
    setElementRequest(requests, 'VehicleInput.Type', str(body['veh_car_typecd']))

    # policy input term
    setElementRequest(requests, 'PolicyInput.EffectiveDate', '~EffectiveDate~')
    # policy input term
    setElementRequest(requests, 'PolicyInput.Term', str(body['pol_term_month']))

    # Comprehensive
    setElementRequest(requests, 'PhysicalDamageComprehensiveInput.Deductible', str(body['prm_comp_ded']))
    setElementRequest(requests, 'PhysicalDamageComprehensiveInput.Limit', str(int(body['prm_comp_limit']) * 1000))

    # Collision or Upset
    setElementRequest(requests, 'PhysicalDamageCollisionOrUpsetInput.Deductible', str(body['prm_coll_ded']))
    setElementRequest(requests, 'PhysicalDamageCollisionOrUpsetInput.Limit', str(int(body['prm_coll_limit']) * 1000))

    # Loss of Use By Theft Rental Reinbursement
    setElementRequest(requests, 'LossOfUseByTheftInput.Limit', str(body['prm_loss_limit']))

    # BI
    setElementRequest(requests, 'BodilyInjuryLiabilityInput.Limit', str(int(body['prm_bi_limit']) * 1000))

    # PD
    setElementRequest(requests, 'PropertyDamageLiabilityInput.Limit', str(int(body['prm_pd_limit']) * 1000))

    # Medical Payments
    setElementRequest(requests, 'MedicalPaymentsMilitaryInput.Limit', str(body['prm_medpay_limit']))

    # initalize data
    ET.SubElement(requests, 'ManuScript.initDataRq', {'manuscript': '~ManuScriptID~'})
    # calculate
    getElementRequest(requests, 'data.SubmissionExecute')
    # get Premium
    getElementRequest(requests, 'data.TotalPremium')
    
    ET.SubElement(requests, 'Session.getDocumentRq', {'var.TotalPremiumWritten': 'session/data/TotalPremium/@written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    totalPremiumWritten = ET.SubElement(echoRq, 'TotalPremiumWritten')
    totalPremiumWritten.text = '~TotalPremiumWritten~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.PhysicalDamageComprehensivePremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageComprehensive"]/PremiumWritten'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    physicalDamageComprehensivePremium = ET.SubElement(echoRq, 'PhysicalDamageComprehensivePremium')
    physicalDamageComprehensivePremium.text = '~PhysicalDamageComprehensivePremium~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.PhysicalDamageCollisionOrUpsetPremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageCollisionOrUpset"]/written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    physicalDamageCollisionOrUpsetPremium = ET.SubElement(echoRq, 'PhysicalDamageCollisionOrUpsetPremium')
    physicalDamageCollisionOrUpsetPremium.text = '~PhysicalDamageCollisionOrUpsetPremium~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.LossOfUseByTheftPremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="LossOfUseByTheft"]/written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    lossOfUseByTheftPremium = ET.SubElement(echoRq, 'LossOfUseByTheftPremium')
    lossOfUseByTheftPremium.text = '~LossOfUseByTheftPremium~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.BodilyInjuryLiabilityPremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="BodilyInjuryLiability"]/written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    bodilyInjuryLiabilityPremium = ET.SubElement(echoRq, 'BodilyInjuryLiabilityPremium')
    bodilyInjuryLiabilityPremium.text = '~BodilyInjuryLiabilityPremium~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.PropertyDamageLiabilityPremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PropertyDamageLiability"]/written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    propertyDamageLiabilityPremium = ET.SubElement(echoRq, 'PropertyDamageLiabilityPremium')
    propertyDamageLiabilityPremium.text = '~PropertyDamageLiabilityPremium~'

    ET.SubElement(requests, 'Session.getDocumentRq', {'var.MedicalPaymentsMilitaryPremium': 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="MedicalPaymentsMilitary"]/written'})
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    medicalPaymentsMilitaryPremium = ET.SubElement(echoRq, 'MedicalPaymentsMilitaryPremium')
    medicalPaymentsMilitaryPremium.text = '~MedicalPaymentsMilitaryPremium~'

    getElementRequest(requests, 'PhysicalDamageComprehensiveOutput.PremiumWritten')
    getElementRequest(requests, 'PhysicalDamageCollisionOrUpsetOutput.PremiumWritten')

    # Handling Save Quote
    if save == True:
        ET.SubElement(requests, 'OnlineData.autoSaveDataRq', {'manuscriptID': '~ManuScriptID~', 'var.QuoteID': '@quoteID', 'var.QuoteNumber': '@policyNumber', 'var.HistoryID': '@historyID'})
        # ET.SubElement(requests, 'Session.getDocumentRq', {'var.PolicyNumber': 'session/data/policy/PolicyNumber'})
        ET.SubElement(requests, 'Session.getDocumentRq')
        echoRq = ET.SubElement(requests, 'Request.echoRq')
        quoteID = ET.SubElement(echoRq, 'QuoteID')
        quoteID.text = '~QuoteID~'
        policyNumber = ET.SubElement(echoRq, 'PolicyNumber')
        policyNumber.text = '~PolicyNumber~'
    return ET

def createApplication(quoteid, body):
    
    effectiveDate = body['pol_term_from_dt']
    effectiveDate = re.sub('[/]', '-', effectiveDate)

    # server root
    server = ET.Element('server')
    # requests
    requests = ET.SubElement(server, 'requests')

    # session login request
    ET.SubElement(requests, 'Session.loginRq', {'userName':'admin', 'password':'admin'})

    # set variable
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'EffectiveDate','value': effectiveDate})
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'TransactionType','value': 'New'})

    # load policy 
    ET.SubElement(requests, 'OnlineData.loadPolicyRq', {
        'policyID': quoteid,
        'var.ManuScriptID': '@manuScriptID'
    })

    # ET.SubElement(requests, 'TransACT.startTransactionRq', {
    #     'code': '~TransactionType~',
    #     'effectiveDate': '~EffectiveDate~',
    #     'allowCheckoutOverride': '1' 
    # })

    # ET.SubElement(requests, 'TransACT.commitTransactionRq')

    # autoSave
    ET.SubElement(requests, 'OnlineData.autoSaveDataRq', {
        'manuscriptID': '~ManuScriptID~', 
        'var.QuoteID': '@quoteID', 
        'var.QuoteNumber': '@policyNumber', 
        'var.HistoryID': '@historyID'
    })

    # initalize data
    # ET.SubElement(requests, 'ManuScript.initDataRq', {'manuscript': '~ManuScriptID~'})

    # calculate
    getElementRequest(requests, 'data.SubmissionExecute')

    ET.SubElement(requests, 'ManuScript.getValueRq', {
        'manuscript': '~ManuScriptID~', 
        'field': 'PolicyAdmin.WorkflowManuScriptID', 
        'var.WorkflowManuScriptID': '@value'
    })
    ET.SubElement(requests, 'ManuScript.getPageRq', {
        'command': 'goto', 
        'topic': 'MainInterview', 
        'page': 'Account', 
        'preEventFieldConfirm': '0', 
        'preEventFieldConfirmText': '', 
        'preEventField': 'Application.PreAction', 
        'manuscript': '~ManuScriptID~', 
        'preEventManuscript': '~WorkflowManuScriptID~'
    })

    ET.SubElement(requests, 'TransACT.getTransactionStatusRq', {
        'historyID': '~HistoryID~', 
        'allowCheckoutOverride': '1'
    })
    ET.SubElement(requests, 'TransACT.commitTransactionRq', {
        'historyID': '~HistoryID~', 
        'allowCheckoutOverride': '1'
    })

    ET.SubElement(requests, 'Session.getDocumentRq', {
        'var.PolicyNumber': 'session/data/policy/PolicyNumber',
        'var.TotalPremiumWritten': 'session/data/TotalPremium/@written',
        'var.TotalPremiumChange': 'session/data/TotalPremium/@change'
    })

    propList = {'QuoteID', 'HistoryID', 'PolicyNumber', 'TotalPremiumWritten', 'TotalPremiumChange'}

    echoRq = ET.SubElement(requests, 'Request.echoRq')
    for key in propList:
        prop = ET.SubElement(echoRq, key)
        prop.text = '~' + key +'~'

    ET.SubElement(requests, 'Session.closeRq')

    req_xml = ET.tostring(server, encoding='utf8', method='xml')
    print('-----------------------Request Start-------------------------')
    print(prettify(server))
    print('-----------------------Request End-------------------------')
    print('\n')

    url = 'http://motr12ascp010.eastus2.cloudapp.azure.com/DuckCreek_MO_191113/DCTServer.aspx'
    req = urllib.request.Request(url, data=req_xml)

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    with urllib.request.urlopen(req, context=context) as res:
        res_xml = res.read()
        elem = ET.fromstring(res_xml)
        print('-----------------------Response Start-------------------------')
        print(prettify(elem))
        print('-----------------------Response End-------------------------')
        print('\n')
    return body

def updateQuote(quoteid, body):
    
    manuscript = 'R98a475d7-566b-4b71-92c2-e28d011770e'

    # server root
    server = ET.Element('server')

    # requests
    requests = ET.SubElement(server, 'requests')

    # session login request
    ET.SubElement(requests, 'Session.loginRq', {'userName':'admin', 'password':'admin'})
    # # set variable
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'ManuScriptID','value': manuscript})

    ET.SubElement(requests, 'OnlineData.loadPolicyRq', {'policyID': quoteid})

    setVariablesRequest(ET, requests, manuscript, body, False)

    # # autoSaveDataRq = ET.SubElement(requests, 'OnlineData.autoSaveDataRq', {'manuscriptID': '~ManuScriptID~', 'clientID': '08D7ABAF8A9BB63C','quoteID': quoteid, 'sessionOnly': 'true'})
    # ET.SubElement(requests, 'OnlineData.storePolicyRq', {'manuScriptID': '~ManuScriptID~', 'policyID': quoteid})
    # # ET.SubElement(autoSaveDataRq, "field", {'manuscriptID': '~ManuScriptID~', 'fieldRef': 'AccountInput.MilitaryRank', 'value': body['ins_rank']})

    # session close
    ET.SubElement(requests, 'Session.closeRq')
    print(ET)

    req_xml = ET.tostring(server, encoding='utf8', method='xml')
    print('-----------------------Request Start-------------------------')
    print(prettify(server))
    print('-----------------------Request End-------------------------')
    print('\n')

    url = 'http://motr12ascp010.eastus2.cloudapp.azure.com/DuckCreek_MO_191113/DCTServer.aspx'
    req = urllib.request.Request(url, data=req_xml)

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    with urllib.request.urlopen(req, context=context) as res:
        res_xml = res.read()
        elem = ET.fromstring(res_xml)
        print('-----------------------Response Start-------------------------')
        print(prettify(elem))
        print('-----------------------Response End-------------------------')
        print('\n')
    return body

def getQuote(quoteid):
    return doGetQuote(quoteid, False)

def doGetQuote(quoteid, save=False):

    manuscript = 'R98a475d7-566b-4b71-92c2-e28d011770e'

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)

    # server root
    server = ET.Element('server')

    # requests
    requests = ET.SubElement(server, 'requests')

    # session login request
    ET.SubElement(requests, 'Session.loginRq', {'userName':'admin', 'password':'admin'})
    # set variable
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'ManuScriptID','value': manuscript})

    ET.SubElement(requests, 'OnlineData.loadPolicyRq', {'policyID': quoteid})

    props = dict([
        ('var.ins_name', 'session/data/account/Name'),
        ('var.ins_addr1', 'session/data/account/address/Address1'),
        ('var.ins_addr2', 'session/data/account/address/Address2'),
        # ('var.ins_factor', 'session/data/account/PolicyNumber'),
        ('var.ins_marital', 'session/data/account/MaritalStatus'),
        ('var.ins_tel_home', 'session/data/account/PrimaryPhone'),
        ('var.ins_tel_office', 'session/data/account/PrimaryPhone'),
        ('var.ins_birthdate', 'session/data/account/BirthDate'),
        ('var.ins_age', 'session/data/account/Age'),
        ('var.ins_rank', 'session/data/account/MilitaryRank'),
        ('var.ins_pay_grade', 'session/data/account/MilitaryRank'),
        ('var.pol_term_orig_eff_dt', 'session/data/policy/EffectiveDate'),
        ('var.pol_term_from_dt', 'session/data/policy/EffectiveDate'),
        ('var.pol_term_month', 'session/data/policy/Term'),
        ('var.pol_term_to_dt', 'session/data/policy/ExpirationDate'),
        ('var.policy_number', 'session/data/policy/PolicyNumber'),
        ('var.veh_lic_plate2', 'session/data/account/MilitaryBaseLocation'),
        ('var.veh_car_typecd', 'session/data/policy/line[Type="PersonalAuto"]/risk/vehicle/Type'),

        ('var.prm_comp_ded', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageComprehensive"]/Deductible'),
        ('var.prm_comp_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageComprehensive"]/limit[Type="PhysicalDamageComprehensive"]/Limit'),
        ('var.prm_comp_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageComprehensive"]/PremiumWritten'),
        
        ('var.prm_coll_ded', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageCollisionOrUpset"]/Deductible'),
        ('var.prm_coll_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageCollisionOrUpset"]/limit[Type="PhysicalDamageCollisionOrUpset"]/Limit'),
        ('var.prm_coll_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PhysicalDamageCollisionOrUpset"]/written'),

        ('var.prm_loss_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="LossOfUseByTheft"]/limit[Type="LossOfUseByTheft"]/Limit'),
        ('var.prm_loss_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="LossOfUseByTheft"]/written'),

        ('var.prm_bi_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="BodilyInjuryLiability"]/limit[Type="BodilyInjuryLiability"]/Limit'),
        ('var.prm_bi_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="BodilyInjuryLiability"]/written'),

        ('var.prm_pd_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PropertyDamageLiability"]/limit[Type="PropertyDamageLiability"]/Limit'),
        ('var.prm_pd_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="PropertyDamageLiability"]/written'),

        ('var.prm_medpay_limit', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="MedicalPaymentsMilitary"]/limit[Type="MedicalPaymentsMilitary"]/Limit'),
        ('var.prm_medpay_premium', 'session/data/policy/line[Type="PersonalAuto"]/risk/coverage[Type="MedicalPaymentsMilitary"]/written'),

        ('var.prm_gross_premium', 'session/data/TotalPremium/@written'),
        ('var.prm_net_premium', 'session/data/TotalPremium/@written'),
        ('var.prm_total_premium', 'session/data/TotalPremium/@written'),
    ])

    ET.SubElement(requests, 'Session.getDocumentRq', props)
    echoRq = ET.SubElement(requests, 'Request.echoRq')
    for key in props.keys():
        prop = ET.SubElement(echoRq, key[4:])
        prop.text = '~' + key[4:] +'~'
    
    # session close
    ET.SubElement(requests, 'Session.closeRq')

    req_xml = ET.tostring(server, encoding='utf8', method='xml')
    print('-----------------------Request Start-------------------------')
    print(prettify(server))
    print('-----------------------Request End-------------------------')
    print('\n')

    url = 'http://motr12ascp010.eastus2.cloudapp.azure.com/DuckCreek_MO_191113/DCTServer.aspx'
    req = urllib.request.Request(url, data=req_xml)

    with urllib.request.urlopen(req, context=context) as res:
        res_xml = res.read()
        elem = ET.fromstring(res_xml)
        print('-----------------------Response Start-------------------------')
        print(prettify(elem))
        print('-----------------------Response End-------------------------')
        print('\n')

        keys = []
        values = []
        for key in props.keys():
            keys.append(key[4:])
            values.append(elem.find('.//' + key[4:]).text)
        
        retQuote = dict(zip(keys, values))
        retQuote['prm_comp_limit'] = int(retQuote['prm_comp_limit']) / 1000
        retQuote['prm_coll_limit'] = int(retQuote['prm_coll_limit']) / 1000
        retQuote['prm_bi_limit'] = int(retQuote['prm_bi_limit']) / 1000
        retQuote['prm_pd_limit'] = int(retQuote['prm_pd_limit']) / 1000
        retQuote['quote_id'] = quoteid
        print(retQuote)
    return retQuote


def getAllQuotes(quoteid=None, skip=None, limit=None):

    manuscript = 'R98a475d7-566b-4b71-92c2-e28d011770e'

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)

    # server root
    server = ET.Element('server')

    # requests
    requests = ET.SubElement(server, 'requests')

    # session login request
    ET.SubElement(requests, 'Session.loginRq', {'userName':'admin', 'password':'admin', 'cansave': '1'})
    # set variable
    ET.SubElement(requests, 'Request.setVarRq', {'name': 'ManuScriptID','value': manuscript})

    # online load list
    listRq = ET.SubElement(requests, 'OnlineData.listRq', {'startIndex': '1', 'maxItems': '10', 'listAll': '1'})
    keys = ET.SubElement(listRq, 'keys')
    ET.SubElement(keys, 'key', {'name': 'manuScriptID', 'op': 'begins', 'value': 'R98a'})
    ET.SubElement(keys, 'key', {'name': 'status', 'op': 'equal', 'value': 'Quote'})
    orderKeys = ET.SubElement(keys, 'orderKeys')
    ET.SubElement(orderKeys, 'orderKey', {'name': 'creationDate', 'direction': 'DESC'})

    # session close
    ET.SubElement(requests, 'Session.closeRq')

    req_xml = ET.tostring(server, encoding='utf8', method='xml')
    print('-----------------------Request Start-------------------------')
    print(prettify(server))
    print('-----------------------Request End-------------------------')
    print('\n')

    url = 'http://motr12ascp010.eastus2.cloudapp.azure.com/DuckCreek_MO_191113/DCTServer.aspx'
    req = urllib.request.Request(url, data=req_xml)

    with urllib.request.urlopen(req, context=context) as res:
        res_xml = res.read()
        elem = ET.fromstring(res_xml)
        print('-----------------------Response Start-------------------------')
        print(prettify(elem))
        print('-----------------------Response End-------------------------')
        print('\n')
        
        quoteList = []
        for result in elem.iter('policy'):
            keys = ['policy_number', 'pol_date', 'quote_id', 'ins_name', 'txn_status', 'pol_term_from_dt']
            values = [result.attrib['policyNumber'], result.attrib['creationDate'][:11], result.attrib['policyID'], result.attrib['partyName'], result.attrib['status'], result.attrib['effectiveDate']]
            quote = dict(zip(keys, values))
            quoteList.append(quote)
        
        print(quoteList)

    return quoteList

def calculatePremium(body=None):
    return doCalculatePremium(body, False)

def doCalculatePremium(body=None, save=False):

    print(body)

    if connexion.request.is_json:
        manuscript = 'R98a475d7-566b-4b71-92c2-e28d011770e'

        effectiveDate = body['pol_term_from_dt']
        effectiveDate = re.sub('[/]', '-', effectiveDate)

        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)

        # server root
        server = ET.Element('server')

        # requests
        requests = ET.SubElement(server, 'requests')

        # session login request
        ET.SubElement(requests, 'Session.loginRq', {'userName':'admin', 'password':'admin'})

        setVariablesRequest(ET, requests, manuscript, body, save)

        # session close
        ET.SubElement(requests, 'Session.closeRq')

        req_xml = ET.tostring(server, encoding='utf8', method='xml')
        print('-----------------------Request Start-------------------------')
        print(prettify(server))
        print('-----------------------Request End-------------------------')
        print('\n')

        url = 'http://motr12ascp010.eastus2.cloudapp.azure.com//DuckCreek_MO_191113/DCTServer.aspx'
        req = urllib.request.Request(url, data=req_xml)

        totalPremiumWritten = 0
        physicalDamageComprehensivePremium = 0
        physicalDamageCollisionOrUpsetPremium = 0
        lossOfUseByTheftPremium = 0
        bodilyInjuryLiabilityPremium = 0
        propertyDamageLiabilityPremium = 0
        medicalPaymentsMilitaryPremium = 0
        with urllib.request.urlopen(req, context=context) as res:
            res_xml = res.read()
            elem = ET.fromstring(res_xml)
            print('-----------------------Response Start-------------------------')
            print(prettify(elem))
            print('-----------------------Response End-------------------------')
            print('\n')

            totalPremiumWritten = elem.find('.//TotalPremiumWritten').text
            physicalDamageComprehensivePremium = elem.find('.//PhysicalDamageComprehensivePremium').text
            physicalDamageCollisionOrUpsetPremium = elem.find('.//PhysicalDamageCollisionOrUpsetPremium').text
            lossOfUseByTheftPremium = elem.find('.//LossOfUseByTheftPremium').text
            bodilyInjuryLiabilityPremium = elem.find('.//BodilyInjuryLiabilityPremium').text
            propertyDamageLiabilityPremium = elem.find('.//PropertyDamageLiabilityPremium').text
            medicalPaymentsMilitaryPremium = elem.find('.//MedicalPaymentsMilitaryPremium').text

            if save == True:
                quoteID = elem.find('.//QuoteID').text
                policyNumber = elem.find('.//PolicyNumber').text
                print(quoteID)
                print(policyNumber)
                body['quote_id'] = int(quoteID)
                body['policy_number'] = policyNumber

            print(totalPremiumWritten)
            print(physicalDamageComprehensivePremium)
            print(physicalDamageCollisionOrUpsetPremium)
            print(lossOfUseByTheftPremium)
            print(bodilyInjuryLiabilityPremium)
            print(propertyDamageLiabilityPremium)
            print(medicalPaymentsMilitaryPremium)

            body['prm_comp_premium'] = int(physicalDamageComprehensivePremium)
            body['prm_coll_premium'] = int(physicalDamageCollisionOrUpsetPremium)

            body['prm_loss_premium'] = int(lossOfUseByTheftPremium)
            body['prm_bi_premium'] = int(bodilyInjuryLiabilityPremium)
            body['prm_pd_premium'] = int(propertyDamageLiabilityPremium)
            body['prm_medpay_premium'] = int(medicalPaymentsMilitaryPremium)

            body['prm_gross_premium'] = int(totalPremiumWritten)
            body['prm_net_premium'] = int(totalPremiumWritten)
            body['prm_total_premium'] = int(totalPremiumWritten)
    return body

def saveQuote(body=None):
    return doCalculatePremium(body, True)

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")